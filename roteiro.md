o que é o spark

o paradigma map reduce, inspirado na programação constite de formulação de computações em duas etapas primordiais, o map e o reduce
seguido de outras etapas intermediarias, shuffle.
a operação map consiste em aplicar uma função unária a cada elemento de um conjunto de dados e o reduce trata da agregação desses dados por uma operação associativa.
é importante ressaltar que todo o processo é distribuido e assincrono, não há qualquer garantia de ordem entre os dados.
Na pratica cada computador trabalha com uma parte do conjunto de dados e se comunicam passando dados de um para o outro apenas para o reduce.
O HDFS parte do ecosistema hadoop, trata de fazer essa abstração da distribuição dos dados, de tal forma que quando ser for trabalhar com um dado o caminho dele é apenas um link



# Carregando as bibliotecas
library(sparklyr)

library(dplyr)

library(nycflights13)

library(ggplot2)

# Conectando ao spark

sc <- spark_connect(master="yarn")

flights <- copy_to(sc, flights, "flights")

airlines <- copy_to(sc, airlines, "airlines")

src_tbls(sc)

# equivalencia SQL

select ~ SELECT

filter ~ WHERE

arrange ~ ORDER

summarise ~ aggregators: sum, min, sd, etc.

mutate ~ operators: +, *, log, etc.

# workaround

select(flights, year:day, arr_delay, dep_delay)

filter(flights, dep_delay > 1000)

arrange(flights, desc(dep_delay))

c4 <- flights %>%
  filter(month == 5, day == 17, carrier %in% c('UA', 'WN', 'AA', 'DL')) %>%
  select(carrier, dep_delay, air_time, distance) %>%
  arrange(carrier) %>%
  mutate(air_time_hours = air_time / 60)

c4 %>%
  group_by(carrier) %>%
  summarize(count = n(), mean_dep_delay = mean(dep_delay))

sample_frac(flights, 0.01)

carrierhours <- collect(c4)

tbl <- spark_read_parquet(sc, "data", "hdfs://hdfs.company.org:9000/hdfs-path/data")

# Hive function
flights %>%
  mutate(flight_date = paste(year,month,day,sep="-"),
         days_since = datediff(current_date(), flight_date)) %>%
  group_by(flight_date,days_since) %>%
  tally() %>%
  arrange(-days_since)